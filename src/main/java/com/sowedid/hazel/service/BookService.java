package com.sowedid.hazel.service;

import java.util.List;

import com.sowedid.hazel.model.Book;
import com.sowedid.hazel.repository.BookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Cacheable(cacheNames = "allBooks")
    public List<Book> fetchAllBooks() {

        return bookRepository.findAll();
    }

    @CachePut(cacheNames = "byIsbn")
    public Book fetchByIsbn(String isbn) {

        return findBookInSlowSource(isbn);
    }

    private Book findBookInSlowSource(String isbn) {
    
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return bookRepository.findByIsbn(isbn);
    }

    
}
