package com.sowedid.hazel.controller;

import java.util.List;

import com.sowedid.hazel.model.Book;
import com.sowedid.hazel.service.BookService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;


    @GetMapping(value = "/byIsbn")
    public ResponseEntity<Book> byIsbn(String isbn) {

        return new ResponseEntity<>(bookService.fetchByIsbn(isbn), 
                                    HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Book>> allBooks() {

        return new ResponseEntity<>(bookService.fetchAllBooks(), 
                                    HttpStatus.ACCEPTED);

    }
    
}
